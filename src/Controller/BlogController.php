<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog_list")
     */
    public function index()
    {

        $path    = '../public/articles';
        $files = scandir($path);
        $files = array_diff(scandir($path), array('.', '..'));

        $articles = array();

        foreach ($files as $file) {
            if ($file[0] != '.') {
                array_push($articles, $file);
            }
        }

        return $this->render('blog/blog_list.html.twig', [
            'articleList' => $articles,
            'series' => 'None',
        ]); 
    }

    /**
     * @Route("/blog/{articlePath}", name="blog_article")
     */
    public function article(string $articlePath)
    {

        $articlePath = "../public/articles/" . $articlePath;
        $myfile = fopen($articlePath, "r") or die("Unable to open file!");

        $articleContent = fread($myfile, filesize($articlePath));
        fclose($myfile);

        return $this->render('blog/blog_content.html.twig', [
            'articleContent' => $articleContent,
            'series' => 'None',
        ]);
    }
        
}
